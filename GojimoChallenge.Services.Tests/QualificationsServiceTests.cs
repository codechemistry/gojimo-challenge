﻿using NUnit.Framework;
using System;
using SQLite.Net.Platform.Generic;
using System.IO;
using Moq;
using GojimoChallenge.Domain;
using System.Collections.Generic;
using SQLite.Net.Interop;

namespace GojimoChallenge.Services.Tests
{
	[TestFixture ()]
	public class QualificationsServiceTests
	{
		QualificationsService service=null;
		const string dbPath = "./test.db";
		const string svcUrl = "https://api.gojimo.net";
		ISQLitePlatform platform = null;

		[SetUp] public void SetUp(){
			platform = new SQLitePlatformGeneric ();
			var webSvc = new GojimoWebService (svcUrl, dbPath, platform);
			service = new QualificationsService (webSvc, dbPath, platform);
		}

		[TearDown] public void TearDown(){
			File.Delete ("./test.db");
			service = null;
			platform = null;
		}


		[Test] async public void Should_GetAll_Qualifications_When_Refreshed()
		{
			var result = await service.GetAll ();
			Assert.IsEmpty (result);

			await service.RefreshAll ();

			result = await service.GetAll ();
			Assert.IsNotEmpty (result);
		}

		[Test] async public void Should_NotUpdateTheData_When_CacheIsStillValid ()
		{
			var firstUpdateHasData = await service.RefreshAll ();
			var secondUpdateHasData = await service.RefreshAll ();
			Assert.IsTrue (firstUpdateHasData);
			Assert.IsFalse (secondUpdateHasData);
		}

		[Test] async public void Should_GetQualificationById_When_IdIsValid(){
			await service.RefreshAll ();
			var item = (await service.GetAll ())[9];
			var result = await service.ById (item.Id);
			Assert.AreEqual (item.Id, result.Id);
			Assert.IsNotEmpty (result.Subjects);
		}

		[Test] async public void Should_ClearTheDatastore_When_CleanIsCalled(){
			await service.RefreshAll ();
			var items = (await service.GetAll ());

			Assert.IsNotEmpty (items);

			await service.ClearAll ();
			var result = await service.GetAll ();

			Assert.IsEmpty (result);
		}
	}
}
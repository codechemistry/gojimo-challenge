﻿using Foundation;
using UIKit;
using GojimoChallenge.Services;
using System;
using System.IO;

namespace GojChall
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
	[Register ("AppDelegate")]
	public class AppDelegate : UIApplicationDelegate, IUISplitViewControllerDelegate
	{
		// class-level declarations

		public override UIWindow Window {
			get;
			set;
		}

		public override bool FinishedLaunching (UIApplication application, NSDictionary launchOptions)
		{
			// Code to start the Xamarin Test Cloud Agent
			#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start();
			#endif

			var sqlitePlatform = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS ();
			var appDir = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.LocalApplicationData), ".gojimo");
			if (!Directory.Exists (appDir)) {
				Directory.CreateDirectory (appDir);
			}
			ServiceFactory.Init (appDir, sqlitePlatform).Wait ();
			// Override point for customization after application launch.
			var splitViewController = (UISplitViewController)Window.RootViewController;
			var navigationController = (UINavigationController)splitViewController.ViewControllers [1];
			navigationController.TopViewController.NavigationItem.LeftBarButtonItem = splitViewController.DisplayModeButtonItem;
			splitViewController.WeakDelegate = this;
			return true;
		}

		[Export ("splitViewController:collapseSecondaryViewController:ontoPrimaryViewController:")]
		public bool CollapseSecondViewController (UISplitViewController splitViewController, UIViewController secondaryViewController, UIViewController primaryViewController)
		{
			if (secondaryViewController.GetType () == typeof(UINavigationController) &&
			    ((UINavigationController)secondaryViewController).TopViewController.GetType () == typeof(DetailViewController) &&
			    ((DetailViewController)((UINavigationController)secondaryViewController).TopViewController).DetailItem == null) {
				// Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
				return true;
			}
			return false;
		}
	}
}
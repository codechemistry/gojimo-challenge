﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using UIKit;
using Foundation;
using CoreGraphics;
using BigTed;

using GojimoChallenge.Services;
using GojimoChallenge.Domain;

namespace GojChall
{
	public partial class MasterViewController : UITableViewController
	{
		static readonly NSString CellIdentifier = new NSString ("Cell");
		public List<Qualification> Items{ get; private set;} = new List<Qualification> ();
		DetailViewController DetailViewController { get; set; }

		public MasterViewController (IntPtr handle) : base (handle)
		{
			Title = NSBundle.MainBundle.LocalizedString ("Qualifications", "Qualifications");
			if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
				PreferredContentSize = new CGSize (320f, 600f);
				ClearsSelectionOnViewWillAppear = false;
			}
		}

		async public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			DetailViewController = (DetailViewController)((UINavigationController)SplitViewController.ViewControllers [1]).TopViewController;
			var isAlreadyLoaded = NSUserDefaults.StandardUserDefaults.BoolForKey ("AlreadyLoaded");
			if (!isAlreadyLoaded) {
				await LoadData (true, "Loading data for the first time");
				NSUserDefaults.StandardUserDefaults.SetBool (true, "AlreadyLoaded");
			} else {
				await LoadData ();
			}
		}

		async public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			if (segue.Identifier == "showDetail") {
				var indexPath = TableView.IndexPathForSelectedRow;
				var item = Items [indexPath.Row];
				var controller = (DetailViewController)((UINavigationController)segue.DestinationViewController).TopViewController;
				await controller.SetDetailItem (item);
				controller.NavigationItem.LeftBarButtonItem = SplitViewController.DisplayModeButtonItem;
				controller.NavigationItem.LeftItemsSupplementBackButton = true;
			}
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return Items!=null?Items.Count:0;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (CellIdentifier, indexPath);
			cell.TextLabel.Text = Items [indexPath.Row].Name;
			return cell;
		}

		async public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
				await DetailViewController.SetDetailItem (Items [indexPath.Row]);
		}

		async partial void onRefreshClicked (NSObject sender)
		{
			await LoadData (true);
		}

		async Task LoadData(bool refresh=false, string displayMessage=null){
			var needsReload = true;
			if (refresh) {
				try {
					BTProgressHUD.Show ("Loading Data", -1, ProgressHUD.MaskType.Gradient);
					needsReload = await ServiceFactory.Qualifications.RefreshAll ();	
				} catch (WebException) {
					ShowError ("No Internet connection available", "Please connect your device to internet in order to refresh the data");
				} catch (Exception) {
					//TODO: Log the exception properly...
					ShowError ("Upps, I feel ashamed","Unrecognized error happened\nThings like this can happen on a draft app");
				} finally {
					BTProgressHUD.Dismiss ();
				}
			}
			if(needsReload){
				Items = await ServiceFactory.Qualifications.GetAll ();
				TableView.ReloadData ();
			}
		}

		static void ShowError(string title, string text){
			new UIAlertView (title, text, null, "OK").Show ();
		}

		protected override void Dispose (bool disposing)
		{
			if (disposing) {
				DetailViewController = null;
				Items.Clear ();
				Items = null;
			}
			base.Dispose (disposing);
		}
	}
}
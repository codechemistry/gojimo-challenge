﻿using System;
using UIKit;
using CoreGraphics;

namespace GojChall
{
	public static class Extensions
	{
		public static void ShowNoItems(this UITableView tableView, string message){
			var rect = new CGRect (0, 0, tableView.Bounds.Size.Width, tableView.Bounds.Size.Height);
			var lblNoContent = new UILabel (rect);
			lblNoContent.Text = message;
			lblNoContent.TextAlignment = UITextAlignment.Center;
			tableView.BackgroundView = lblNoContent;
			tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
		}

		public static UIColor ToUIColor(this String hexString, float alpha=1.0f){
			return String.IsNullOrEmpty (hexString) ? 
				UIColor.White : 
				UIColor.Clear.FromHexString (hexString, alpha);
			
		}
		public static UIColor FromHexString (this UIColor color, string hexValue, float alpha = 1)
		{
			float red, green, blue;
			var colorString = hexValue.Replace ("#", "");

			alpha = alpha > 1 ? 1 : alpha < 0 ? 0 : alpha;

			switch (colorString.Length) 
			{
			case 3 : // #RGB
				{
					red = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(0, 1)), 16) / 255f;
					green = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(1, 1)), 16) / 255f;
					blue = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(2, 1)), 16) / 255f;
					return UIColor.FromRGBA(red, green, blue, alpha);
				}
			case 4: //ARGB
				{
					var currentAlpha =  alpha != 1 ? alpha : Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(0, 1)), 16) / 255f;
					red = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(1, 1)), 16) / 255f;
					green = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(2, 1)), 16) / 255f;
					blue = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(3, 1)), 16) / 255f;
					return UIColor.FromRGBA(red, green, blue, currentAlpha);
				}
			case 6 : //RRGGBB
				{
					red = Convert.ToInt32(colorString.Substring(0, 2), 16) / 255f;
					green = Convert.ToInt32(colorString.Substring(2, 2), 16) / 255f;
					blue = Convert.ToInt32(colorString.Substring(4, 2), 16) / 255f;
					return UIColor.FromRGBA(red, green, blue, alpha);
				}
			case 8: //AARRGGBB
				{
					var currentAlpha = alpha != 1 ? alpha : Convert.ToInt32 (string.Format ("{0}", colorString.Substring (0, 2)), 16) / 255f;
					red = Convert.ToInt32(string.Format("{0}", colorString.Substring(2, 2)), 16) / 255f;
					green = Convert.ToInt32(string.Format("{0}", colorString.Substring(4, 2)), 16) / 255f;
					blue = Convert.ToInt32(string.Format("{0}", colorString.Substring(6, 2)), 16) / 255f;
					return UIColor.FromRGBA(red, green, blue, currentAlpha);
				}
			default :
				throw new ArgumentOutOfRangeException(string.Format("Invalid color value {0} is invalid. It should be a hex value of the form #RBG, #RRGGBB", hexValue));

			}
		}
	}
}
﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Foundation;
using UIKit;

using GojimoChallenge.Domain;
using GojimoChallenge.Services;

namespace GojChall
{
	public partial class DetailViewController : UITableViewController
	{
		static readonly NSString CellIdentifier = new NSString ("SubjectCell");

		public Qualification DetailItem { get; set; }
		List<Subject> Items { get { return DetailItem?.Subjects ?? new List<Subject> (); } }

		public DetailViewController (IntPtr handle) : base (handle){
		}

		async public Task SetDetailItem (Qualification newDetailItem)
		{
			DetailItem = await ServiceFactory.Qualifications.ById (newDetailItem?.Id);
			if (IsViewLoaded && DetailItem!=null) {
				Title = DetailItem.Name;
				TableView.ReloadData ();
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			TableView.WeakDataSource = this;
			if (DetailItem != null) {
				Title = DetailItem.Name;
				TableView.ReloadData ();
			}
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			if (Items.Count == 0) {
				var msg = DetailItem == null ? "No Subject selected" : String.Format ("{0} has no subjects", DetailItem.Name);
				TableView.ShowNoItems (msg);
				return 0;
			}
			else {
				TableView.BackgroundView = null;
				TableView.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
				return 1;
			}
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return Items!=null?Items.Count:0;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (CellIdentifier, indexPath);
			var subject = Items [indexPath.Row];
			cell.TextLabel.Text = subject.Title;
			cell.BackgroundColor = subject.Colour.ToUIColor ();
			return cell;
		}
	}
}

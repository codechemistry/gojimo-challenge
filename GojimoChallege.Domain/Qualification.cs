using Newtonsoft.Json;
using System.Collections.Generic;
using SQLite.Net.Attributes;

namespace GojimoChallenge.Domain
{
	public class Qualification
	{
		[JsonProperty("id")]
		[PrimaryKey]
		public string Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		string countryId;
		[JsonIgnore]
		public string CountryId {
			get {
				return Country != null ? Country.Code : countryId;
			}
			set {
				countryId = value;
				if (Country != null) {
					Country.Code = value;
				}
			}
		}

		[JsonProperty("country")]
		[Ignore]
		public Country Country { get; set; }

		[JsonProperty("subjects")]
		[Ignore]
		public List<Subject> Subjects { get; set; }

		[JsonProperty("link")]
		public string Link { get; set; }

//		[JsonProperty("default_products")]
//		public List<DefaultProduct> DefaultProducts { get; set; }
	}
}

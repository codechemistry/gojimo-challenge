using Newtonsoft.Json;
using SQLite.Net.Attributes;

namespace GojimoChallenge.Domain
{

	public class Subject
	{
		[JsonProperty("id")]
		[PrimaryKey]
		public string Id { get; set; }

		[JsonProperty("title")]
		public string Title { get; set; }

		[JsonProperty("link")]
		public string Link { get; set; }

		[JsonProperty("colour")]
		public string Colour { get; set; }
	}

}

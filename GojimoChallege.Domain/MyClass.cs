﻿//using System;
//using Newtonsoft.Json;
//using System.Collections.Generic;
//
//namespace GojimoChallege.Domain
//{
//	public class Info
//	{
//		[JsonProperty("meta")]
//		public string Meta { get; set; }
//	}
//
//	public class Asset
//	{
//		[JsonProperty("id")]
//		public string Id { get; set; }
//
//		[JsonProperty("copyright")]
//		public object Copyright { get; set; }
//
//		[JsonProperty("meta")]
//		public string Meta { get; set; }
//
//		[JsonProperty("size")]
//		public int? Size { get; set; }
//
//		[JsonProperty("content_type")]
//		public string ContentType { get; set; }
//
//		[JsonProperty("created_at")]
//		public DateTime CreatedAt { get; set; }
//
//		[JsonProperty("updated_at")]
//		public DateTime UpdatedAt { get; set; }
//
//		[JsonProperty("path")]
//		public string Path { get; set; }
//
//		[JsonProperty("unzipped_base_url")]
//		public string UnzippedBaseUrl { get; set; }
//
//		[JsonProperty("info")]
//		public List<Info> Info { get; set; }
//
//		[JsonProperty("link")]
//		public string Link { get; set; }
//
//	}
//
//	public class Publisher
//	{
//		[JsonProperty("id")]
//		public string Id { get; set; }
//
//		[JsonProperty("title")]
//		public string Title { get; set; }
//
//		[JsonProperty("link")]
//		public string Link { get; set; }
//
//	}
//
//	public class DefaultProduct
//	{
//		[JsonProperty("id")]
//		public string Id { get; set; }
//
//		[JsonProperty("title")]
//		public string Title { get; set; }
//
//		[JsonProperty("link")]
//		public string Link { get; set; }
//
//		[JsonProperty("ios_iap_id")]
//		public string IosIapId { get; set; }
//
//		[JsonProperty("store_ids")]
//		public List<object> StoreIds { get; set; }
//
//		[JsonProperty("type")]
//		public string Type { get; set; }
//
//		[JsonProperty("assets")]
//		public List<Asset> Assets { get; set; }
//
//		[JsonProperty("publisher")]
//		public Publisher Publisher { get; set; }
//
//		[JsonProperty("author")]
//		public string Author { get; set; }
//
//	}
//}


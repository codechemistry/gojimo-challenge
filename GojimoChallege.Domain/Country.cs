using System;
using Newtonsoft.Json;
using SQLite.Net.Attributes;

namespace GojimoChallenge.Domain
{

	public class Country
	{
		[JsonProperty("code")]
		[PrimaryKey]
		public string Code { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("created_at")]
		public DateTime CreatedAt { get; set; }

		[JsonProperty("updated_at")]
		public DateTime UpdatedAt { get; set; }

		[JsonProperty("link")]
		public string Link { get; set; }

		public override bool Equals (object obj)
		{
			var second = obj as Country;
			if (second == null)
				return false;
			return second.Code == this.Code;
		}
		public override int GetHashCode ()
		{
			return Code.GetHashCode ();
		}
	}

}

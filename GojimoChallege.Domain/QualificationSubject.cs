using System;
using SQLite.Net.Attributes;

namespace GojimoChallenge.Domain
{
	public class QualificationSubject{
		[Indexed(Name = "QualificationSubjectID", Order = 1, Unique = true)]
		public string QualificationId{get; set;}
		[Indexed(Name = "QualificationSubjectID", Order = 2, Unique = true)]
		public string SubjectId{get; set;}
	}
	
}
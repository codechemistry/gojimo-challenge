﻿using System;

namespace GojimoChallege.Domain
{
	public static class ObjectHelpers
	{
		public static bool IsNull(this Object item){
			return item == null;
		}

		public static bool IsNotNull(this Object item){
			return item != null;
		}

		public static bool IsNotNullOrEmpty(this string item){
			return !String.IsNullOrEmpty (item);
		}
	}
}


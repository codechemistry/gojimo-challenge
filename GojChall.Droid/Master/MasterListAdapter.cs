﻿using Android.Widget;
using Android.App;
using GojimoChallenge.Domain;

namespace GojChall.Droid
{
	public class MasterListAdapter : BaseListAdapter<Qualification>
	{
		public MasterListAdapter (Activity context):base(context)
		{			
		}			

		public override Android.Views.View GetView (int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			ViewHolder holder = null;
			var view = convertView;

			if (view != null)
				holder = view.Tag as ViewHolder;

			if (holder == null) {
				holder = new ViewHolder ();
				view = context.LayoutInflater.Inflate (Resource.Layout.ListItem, null);
				holder.Text = view.FindViewById<TextView> (Resource.Id.tvText);
				view.Tag = holder;
			}
			holder.Text.Text = this [position].Name;
			return view;
		}
	}
}


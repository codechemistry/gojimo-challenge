﻿using Android.App;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using GojimoChallenge.Services;
using System.Net;
using System;
using GojimoChallenge.Domain;
using System.Collections.Generic;
using Android.Content;
using Android.Views;

namespace GojChall.Droid
{
	[Activity (Label = "@string/qualifications", MainLauncher = true, Icon = "@mipmap/ic_menu_white_hamburger")]
	public class MasterActivity : ListActivity
	{
		MasterListAdapter adapter;	

		protected async override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			ActionBar.SetDisplayShowHomeEnabled(false);
			adapter = new MasterListAdapter (this);
			await LoadData (true);
			ListAdapter = adapter;
		}

		protected override void OnListItemClick (ListView l, Android.Views.View v, int position, long id)
		{
			var item = adapter [position];
			var intent = new Intent (this, typeof(DetailsActivity));
			intent.PutExtra (DetailsActivity.EXTRA_ITEM_ID, item.Id);
			StartActivity (intent);
		}

		async Task LoadData(bool isInit = false){
			var needsReload = true;
			ProgressDialog progressDialog = null;
			try {
				var loadingText = GetText (Resource.String.loading_data);
				progressDialog = ProgressDialog.Show (this, loadingText, loadingText);
				needsReload = await ServiceFactory.Qualifications.RefreshAll ();
			} catch (WebException) {
				ShowToast (Resource.String.connect_to_internet);
			} catch (Exception) {
				ShowToast (Resource.String.unrecognized_error);
			} finally {
				progressDialog.Dismiss ();
			}
			if (needsReload || isInit) {
				adapter.Data = await ServiceFactory.Qualifications.GetAll ();
			}
		}

		void ShowToast(int resourceId){
			Toast.MakeText (this, resourceId, ToastLength.Long).Show ();
		}

		public override bool OnCreateOptionsMenu (Android.Views.IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.BaseMenu, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			OnOptionsItemSelectedAsync (item);
			return true;
		}
		private async Task OnOptionsItemSelectedAsync(IMenuItem item){			
			if (item.ItemId == Resource.Id.btnRefresh) {
				await LoadData ();
			}
			base.OnOptionsItemSelected (item);
		}
	}
}



﻿using System;
using Android.App;
using System.IO;
using Android.Runtime;
using GojimoChallenge.Services;

namespace GojChall.Droid
{
	[Application()]
	public class GojimoApplication : Application
	{
		public GojimoApplication(IntPtr handle, JniHandleOwnership transfer)
			: base(handle, transfer)
		{
		}
		public override void OnCreate ()
		{
			base.OnCreate ();
			var sqlitePlatform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid ();
			var appDir = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.LocalApplicationData), ".gojimo");
			if (!Directory.Exists (appDir)) {
				Directory.CreateDirectory (appDir);
			}
			ServiceFactory.Init (appDir, sqlitePlatform).Wait ();
		}
	}
}


﻿
using System;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;

using GojimoChallenge.Services;
using GojimoChallenge.Domain;

namespace GojChall.Droid
{
	[Activity (Label = "@string/subjects", Icon = "@mipmap/ic_menu_white_hamburger")]			
	public class DetailsActivity : ListActivity
	{
		public const String EXTRA_ITEM_ID = "extra_item_id";
		private Qualification item;
		private DetailsListAdapter adapter;
		protected async override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			ActionBar.SetDisplayHomeAsUpEnabled (true);
			ActionBar.SetDisplayShowHomeEnabled(false);
			SetEmptyView ();
			adapter = new DetailsListAdapter(this);
			await LoadData ();
			ListAdapter = adapter;
		}

		void SetEmptyView ()
		{
			var emptyView = LayoutInflater.Inflate (Resource.Layout.ListEmptyView, null);
			(ListView.Parent as ViewGroup).AddView (emptyView);
			ListView.EmptyView = emptyView;
		}

		async Task LoadData(){
			ProgressDialog progressDialog = null;
			var loadingText = GetText (Resource.String.loading_data);
			progressDialog = ProgressDialog.Show (this, loadingText, loadingText);
			var itemId = Intent.GetStringExtra (EXTRA_ITEM_ID);
			item = await ServiceFactory.Qualifications.ById (itemId);
			if (item!=null) {
				adapter.Data = item.Subjects;
			}
			progressDialog.Dismiss ();
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			if (item.ItemId == Android.Resource.Id.Home) {
				this.Finish ();
			}
			return base.OnOptionsItemSelected (item);
		}
			
	}
}


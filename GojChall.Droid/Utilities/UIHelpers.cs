﻿using System;
using Android.Graphics.Drawables;
using Android.Graphics;
namespace GojChall.Droid
{
	public static class UIHelpers
	{
		public static ColorDrawable ToColorDrawable(this string hexColor){
			Color color = Color.White;
			if (!String.IsNullOrEmpty (hexColor)) {
				color = Color.ParseColor (hexColor);
			}
			return new ColorDrawable (color);
		}
	}
}


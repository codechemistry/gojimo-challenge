﻿using System;
using Android.Widget;
using Android.App;
using System.Collections.Generic;

namespace GojChall.Droid
{

	public abstract class BaseListAdapter<T> : BaseAdapter<T>
	{
		protected Activity context;
		public BaseListAdapter (Activity context)
		{
			this.context = context;
		}			

		List<T> data;
		public List<T> Data {
			get {
				return data;
			}
			set {
				data = value;
				NotifyDataSetChanged ();
			}
		}

		public override long GetItemId (int position)
		{
			return position;
		}			

		public override int Count {
			get {
				return data != null ? data.Count : 0;
			}
		}			

		public override T this [int index] {
			get {
				return data [index];
			}
		}
	}		
}


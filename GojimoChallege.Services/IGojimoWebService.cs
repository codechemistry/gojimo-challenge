using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace GojimoChallenge.Services
{
	public interface IGojimoWebService
	{
		Task<ServiceResponse<T>> Get<T> (string resource) where T:new();
	}
}
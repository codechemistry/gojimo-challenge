﻿using System;
using System.IO;
using System.Threading.Tasks;
using SQLite.Net.Interop;


namespace GojimoChallenge.Services
{
	public static class ServiceFactory
	{
		public static IGojimoWebService WebService{ get; private set; }
		public static QualificationsService Qualifications{get; private set;}
		public static string DatabasePath{ get; private set; }
		public static string RootDirectory{ get; private set; }
		async public static Task Init(string appDir, ISQLitePlatform sqlitePatform){
			RootDirectory = appDir;
			DatabasePath = Path.Combine (RootDirectory, "gojimo.db");
			WebService = new GojimoWebService ("https://api.gojimo.net", DatabasePath, sqlitePatform);
			Qualifications = new QualificationsService (WebService, DatabasePath, sqlitePatform);
			await Task.FromResult (false);
		}
	}

}


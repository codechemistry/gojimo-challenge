﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GojimoChallenge.Services
{
	public static class Extensions
	{
		public static IEnumerable<T> ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
		{
			foreach(T item in enumeration)
			{
				action(item);
				yield return item;
			}
		}

		public static async Task<T> GetContent<T>(this HttpResponseMessage response) where T: new(){
			var content = await response.Content.ReadAsStringAsync ();
			var result = await Task.Factory.StartNew<T> (
				() => JsonConvert.DeserializeObject<T> (content));
			return result;
		}
	}
}


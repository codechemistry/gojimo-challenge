using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

using SQLite.Net.Interop;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Attributes;

namespace GojimoChallenge.Services
{
	public class SyncToken{
		[PrimaryKey]
		public string Key{get; set;}
		public string Tag{get; set;}
	}

	public class GojimoWebService:IGojimoWebService{
		Uri baseAddress;
		readonly SQLiteAsyncConnection db;

		public GojimoWebService (string baseAddressString, string databasePath, ISQLitePlatform platform)
		{
			if (!Uri.TryCreate (baseAddressString, UriKind.Absolute, out baseAddress)) {
				throw new InvalidCastException ("Invalid Uri passed as paramter");
			}
			db = new SQLiteAsyncConnection (()=>new SQLiteConnectionWithLock(platform, new SQLiteConnectionString (databasePath, true)));
			db.CreateTableAsync<SyncToken> ().Wait ();
		}

		public async Task<ServiceResponse<T>> Get<T>(string resource) where T:new(){
			
			using (var client = new HttpClient ()) {
				var request = new HttpRequestMessage() {
					RequestUri = new Uri(baseAddress.AbsoluteUri + resource),
					Method = HttpMethod.Get,
				};
				var eTag = await GetETag (resource);
				if (eTag != null) {
					request.Headers.IfNoneMatch.Add (eTag);
				}

				var response = await client.SendAsync (request);

				T content = default(T);
				if (response.StatusCode == HttpStatusCode.OK) {
					content = await response.GetContent<T> ();
					await SaveETag (resource, response.Headers.ETag);
				}
				return new ServiceResponse<T> {
					ETag = response.Headers.ETag,
					Code = response.StatusCode,
					Data = content
				};
			}
		}


		async Task<EntityTagHeaderValue> GetETag(string resource){
			var token = await db.Table<SyncToken> ()
				.Where (p => p.Key == resource)
				.FirstOrDefaultAsync ();
			return token != null ? new EntityTagHeaderValue (token.Tag) : null;
		}

		async Task SaveETag(string resource, EntityTagHeaderValue tag){
			await db.InsertOrReplaceAsync (new SyncToken {
				Key = resource,
				Tag = tag.Tag
			});
		}
	}
}
﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using SQLite.Net.Interop;
using SQLite.Net;
using SQLite.Net.Async;

using GojimoChallenge.Domain;

namespace GojimoChallenge.Services
{
	public class QualificationsService
	{
		readonly SQLiteAsyncConnection db;
		IGojimoWebService webService;

		public QualificationsService (IGojimoWebService webService, string databasePath, ISQLitePlatform platform)
		{
			this.webService = webService;
			db = new SQLiteAsyncConnection (()=>new SQLiteConnectionWithLock(platform, new SQLiteConnectionString (databasePath, true)));
			db.CreateTableAsync<Country> ().Wait ();
			db.CreateTableAsync<Qualification> ().Wait ();
			db.CreateTableAsync<Subject> ().Wait ();
			db.CreateTableAsync<QualificationSubject> ().Wait ();
		}

		public Task<List<Qualification>> GetAll(){
			return db.Table<Qualification> ().ToListAsync ();
		}

		public async Task<Qualification> ById(string id){
			if (String.IsNullOrEmpty (id))
				return null;
			
			var res =  await db.GetAsync<Qualification> (id);
			if (res.CountryId != null) {
				res.Country = await db.GetAsync<Country> (res.CountryId);
			}
			var query = String.Format ("select s.* from Subject s\n" +
				" join QualificationSubject qs on s.ID = qs.SubjectId\n" +
				" where qs.QualificationID = '{0}'", id);
			res.Subjects = await db.QueryAsync<Subject> (query);
			return res;
		}

		public async Task<bool> RefreshAll(){
			var response = await webService.Get<List<Qualification>> ("/api/v4/qualifications");
			if (response.Data == null) {
				return false;
			} else {
				await ClearAll ();
				await SaveAll (response.Data);
				return true;
			}
		}

		public async Task SaveAll(IEnumerable<Qualification> qualifications){
			var countries = new Dictionary<string, Country> ();
			var subjects = new Dictionary<string, Subject> ();
			var qsubjects = new List<QualificationSubject> ();
			foreach (var qualification in qualifications) {
				if (qualification.Country != null && !countries.ContainsKey (qualification.Country.Code)) {
					countries [qualification.Country.Code] = qualification.Country;
				}
				foreach (var subject in qualification.Subjects) {
					qsubjects.Add (new QualificationSubject{
						QualificationId = qualification.Id,
						SubjectId = subject.Id
					});
					subjects[subject.Id] = subject;
				}
			}

			await db.InsertOrReplaceAllAsync (qualifications);
			await db.InsertOrReplaceAllAsync (qsubjects);
			await db.InsertOrReplaceAllAsync (subjects.Values);
			await db.InsertOrReplaceAllAsync (countries.Values);
		}

		public async Task ClearAll(){
			await db.DeleteAllAsync<Subject> ();
			await db.DeleteAllAsync<Qualification> ();
			await db.DeleteAllAsync<Country> ();
			await db.DeleteAllAsync<QualificationSubject> ();
		}
	}
}
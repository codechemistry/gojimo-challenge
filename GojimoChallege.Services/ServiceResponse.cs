using System.Net;
using System.Net.Http.Headers;

namespace GojimoChallenge.Services
{
	public class ServiceResponse<T> where T: new(){
		public HttpStatusCode Code{ get; set;}
		public EntityTagHeaderValue ETag{ get; set; }
		public T Data{get; set;}
	}
}